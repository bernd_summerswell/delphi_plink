{*******************************************************}
{ UNIT: ssPlinkTunnel.pas                               }                          
{ Delphi PLink                                          }
{                                                       }
{ Copyright (C) 2016 SummerSwell                        }
{ Author  : bvonfintel                                  } 
{ Created : 2016/10/03 10:02:23 PM                      }
{                                                       }
{*******************************************************}
unit ssPlinkTunnel;

interface

uses
  Classes,
  ssPipedProcess,
  SysUtils;

type
  TssPlinkTunnelOnDebug     = procedure( const ADebugMessage: string ) of object;
  TssPlinkTunnelBeforeStart = TNotifyEvent;
  TssPlinkTunnelAfterStart  = TNotifyEvent;
  TssPlinkTunnelBeforeStop  = TNotifyEvent;
  TssPlinkTunnelAfterStop   = TNotifyEvent;

  // *** -------------------------------------------------------------------------
  // *** CLASS: TssPlinkTunnel
  // *** -------------------------------------------------------------------------
  TssPlinkTunnel = class( TObject )
    private
      FProcess      : TssPipedProcess;
      FStarted      : Boolean;
      FOnDebug      : TssPlinkTunnelOnDebug;
      FName         : string;
      FAfterStart   : TssPlinkTunnelAfterStart;
      FAfterStop    : TssPlinkTunnelAfterStop;
      FBeforeStart  : TssPlinkTunnelBeforeStart;
      FBeforeStop   : TssPlinkTunnelBeforeStop;

      FSSHHost     : string;
      FSSHUsername : string;
      FSSHPort     : string;
      FSSHPassword : string;
      
      FRemoteHost  : string;
      FLocalHost   : string;
      FRemotePort  : string;
      FLocalPort   : string;
      FPathToPlink : string;
      FAddtionalOptions: string;

      procedure DoDebug( const ADebugMessage: string );
      procedure DoBeforeStart();
      procedure DoAfterStart();
      procedure DoBeforeStop();
      procedure DoAfterStop();
    public                                       
      constructor Create();
      destructor Destroy; override;

      function RemoteForward( const ALocalHost: string = '';
                              const ALocalPort: string = '';
                              const ARemoteHost: string = '';
                              const ARemotePort: string = ''): Boolean;
                              
      function LocalForward ( const ARemoteHost: string = '';
                              const ARemotePort: string = '';
                              const ALocalHost: string = '';
                              const ALocalPort: string = ''): Boolean;


      function StopTunnel(): Boolean;
      function IsRunning(): Boolean;

      property Name           : string            read FName write FName;
      property Process        : TssPipedProcess   read FProcess;
      property Started        : Boolean           read FStarted;

      property SSHHost      : string read FSSHHost write FSSHHost;
      property SSHPort      : string read FSSHPort write FSSHPort;
      property SSHUsername  : string read FSSHUsername write FSSHUsername;
      property SSHPassword  : string read FSSHPassword write FSSHPassword;

      property LocalHost    : string read FLocalHost write FLocalHost;
      property LocalPort    : string read FLocalPort write FLocalPort;
      property RemoteHost   : string read FRemoteHost write FRemoteHost;
      property RemotePort   : string read FRemotePort write FRemotePort;

      property AddtionalOptions : string read FAddtionalOptions write FAddtionalOptions;
      property PathToPlink      : string read FPathToPlink write FPathToPlink;

      property OnDebug        : TssPlinkTunnelOnDebug       read FOnDebug       write FOnDebug;
      property BeforeStart    : TssPlinkTunnelBeforeStart   read FBeforeStart   write FBeforeStart;
      property AfterStart     : TssPlinkTunnelAfterStart    read FAfterStart    write FAfterStart;
      property BeforeStop     : TssPlinkTunnelBeforeStop    read FBeforeStop    write FBeforeStop;
      property AfterStop      : TssPlinkTunnelAfterStop     read FAfterStop     write FAfterStop;
  end;
  
implementation

{ TssPlinkTunnel }

{-------------------------------------------------------------------------------
  Procedure: TssPlinkTunnel.Create
  Author:    bvonfintel
  DateTime:  2015.01.15
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
constructor TssPlinkTunnel.Create;
begin
  inherited;
  FProcess      := TssPipedProcess.Create();

  FSSHHost      := '';
  FSSHPort      := '22';
  FSSHUsername  := '';
  FSSHPassword  := '';

  FLocalHost    := 'localhost';
  FLocalPort    := '';
  FRemoteHost   := 'localhost';
  FRemotePort   := '';

  FPathToPlink  := ExtractFilePath( ParamStr( 0 ) ) + '\plink.exe';
  FAddtionalOptions := '-C -4';

  FStarted      := False;
end;

{-------------------------------------------------------------------------------
  Procedure: TssPlinkTunnel.Destroy
  Author:    bvonfintel
  DateTime:  2015.01.15
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
destructor TssPlinkTunnel.Destroy;
begin
  FProcess.Free();
  inherited;
end;

{-------------------------------------------------------------------------------
  Procedure: TssPlinkTunnel.DoAfterStart
  Author:    bvonfintel
  DateTime:  2015.01.16
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
procedure TssPlinkTunnel.DoAfterStart;
begin
  if ( Assigned( FAfterStart ) ) then
    try
      FAfterStart( Self );
    except
    end;
end;

{-------------------------------------------------------------------------------
  Procedure: TssPlinkTunnel.DoAfterStop
  Author:    bvonfintel
  DateTime:  2015.01.16
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
procedure TssPlinkTunnel.DoAfterStop;
begin
  if ( Assigned( FAfterStop ) ) then
    try
      FAfterStop( Self );
    except
    end;
end;

{-------------------------------------------------------------------------------
  Procedure: TssPlinkTunnel.DoBeforeStart
  Author:    bvonfintel
  DateTime:  2015.01.16
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
procedure TssPlinkTunnel.DoBeforeStart;
begin
  if ( Assigned( FBeforeStart ) ) then
    try
      FBeforeStart( Self );
    except
    end;
end;

{-------------------------------------------------------------------------------
  Procedure: TssPlinkTunnel.DoBeforeStop
  Author:    bvonfintel
  DateTime:  2015.01.16
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
procedure TssPlinkTunnel.DoBeforeStop;
begin
  if ( Assigned( FBeforeStop ) ) then
    try
      FBeforeStop( Self );
    except
    end;
end;

{-------------------------------------------------------------------------------
  Procedure: TssPlinkTunnel.DoDebug
  Author:    bvonfintel
  DateTime:  2015.01.15
  Arguments: const ADebugMessage: string
  Result:    None
-------------------------------------------------------------------------------}
procedure TssPlinkTunnel.DoDebug(const ADebugMessage: string);
begin
  if ( Assigned( FOnDebug ) ) then
    try
      FOnDebug( ADebugMessage );
    except
    end;
end;

{-------------------------------------------------------------------------------
  Procedure: TssPlinkTunnel.IsRunning
  Author:    bvonfintel
  DateTime:  2015.01.30
  Arguments: None
  Result:    Boolean
-------------------------------------------------------------------------------}
function TssPlinkTunnel.IsRunning: Boolean;
begin
  Result := FProcess.IsStillRunning();
end;

{-------------------------------------------------------------------------------
  Procedure: TssPlinkTunnel.LocalForward
  Author:    bvonfintel
  DateTime:  2016.10.03
  Arguments: const ARemoteHost, ARemotePort, ALocalHost, ALocalPort: string
  Result:    Boolean
-------------------------------------------------------------------------------}
function TssPlinkTunnel.LocalForward(const ARemoteHost, ARemotePort, ALocalHost, ALocalPort: string): Boolean;
begin
  if ( ALocalHost <> '' ) then FLocalHost := ALocalHost;
  if ( ALocalPort <> '' ) then FLocalPort := ALocalPort;
  if ( ARemoteHost <> '' ) then FRemoteHost := ARemoteHost;
  if ( ARemotePort <> '' ) then FRemotePort := ARemotePort;

  DoBeforeStart();
  try
    FProcess.RunCommand := FPathToPlink;
    FProcess.Parameters := Format( '-l %s -pw %s -N %s -L %s:%s:%s:%s %s',
                                   [ FSSHUsername, FSSHPassword, FAddtionalOptions, FLocalHost, FLocalPort, FRemoteHost, FRemotePort, FSSHHost ] );
    FProcess.ShowWindow := False;
    FProcess.Initialise();
    FProcess.WriteToInputPipe( 'y' );
    FProcess.CreateTheProcess();

    DoDebug( Format( '%s: Tunnel started from %s:%s -> %s:%s (ProcessID: %d)', [ FName, FRemoteHost, FRemotePort, FLocalHost, FLocalPort, FProcess.ProcessID() ]  ));

    FStarted := True;
  finally
    DoAfterStart();
  end;

end;

{-------------------------------------------------------------------------------
  Procedure: TssPlinkTunnel.RemoteForward
  Author:    bvonfintel
  DateTime:  2016.10.03
  Arguments: const ALocalHost, ALocalPort, ARemoteHost, ARemotePort: string
  Result:    Boolean
-------------------------------------------------------------------------------}
function TssPlinkTunnel.RemoteForward(const ALocalHost, ALocalPort, ARemoteHost, ARemotePort: string): Boolean;
begin
  if ( ALocalHost <> '' ) then FLocalHost := ALocalHost;
  if ( ALocalPort <> '' ) then FLocalPort := ALocalPort;
  if ( ARemoteHost <> '' ) then FRemoteHost := ARemoteHost;
  if ( ARemotePort <> '' ) then FRemotePort := ARemotePort;

  DoBeforeStart();
  try
    FProcess.RunCommand := FPathToPlink;
    FProcess.Parameters := Format( '-l %s -pw %s -N %s -R %s:%s:%s:%s %s',
                                   [ FSSHUsername, FSSHPassword, FAddtionalOptions, FRemoteHost, FRemotePort, FLocalHost, FLocalPort, FSSHHost ] );
    FProcess.ShowWindow := False;
    FProcess.Initialise();
    FProcess.WriteToInputPipe( 'y' );
    FProcess.CreateTheProcess();

    DoDebug( Format( '%s: Tunnel started from %s:%s -> %s:%s (ProcessID: %d)', [ FName, FLocalHost, FLocalPort, FRemoteHost, FRemotePort, FProcess.ProcessID() ]  ));

    FStarted := True;
  finally
    DoAfterStart();
  end;
end;


{-------------------------------------------------------------------------------
  Procedure: TssPlinkTunnel.StopTunnel
  Author:    bvonfintel
  DateTime:  2015.01.15
  Arguments: None
  Result:    Boolean
-------------------------------------------------------------------------------}
function TssPlinkTunnel.StopTunnel: Boolean;
begin
  DoBeforeStop();
  try
    FProcess.TerminateTheProcess();
    FProcess.Finalise();
    DoDebug( Format( '%s: Tunnel stopped', [ FName ]  ));
    FStarted := False;
  finally
    DoAfterStop();
  end;
end;

end.
