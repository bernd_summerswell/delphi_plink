{*******************************************************}
{ UNIT: ssPipedProcess.pas                              }                          
{ Delphi PLink                                          }
{                                                       }
{ Copyright (C) 2016 SummerSwell                        }
{ Author  : bvonfintel                                  } 
{ Created : 2016/10/03 07:24:59 PM                      }
{                                                       }
{*******************************************************}
unit ssPipedProcess;

interface

uses
  Windows
  {$IFDEF USEJWA}
  , JwaWinBase
  , JwaWtsApi32
  {$ENDIF}
  ;

type
  TssRunType = ( rtCurrentUser, rtSpecifiedUser {$IFDEF USEJWA}, rtConsoleUser {$ENDIF} );

  // *** -------------------------------------------------------------------------
  // *** CLASS: TssPipedProcess
  // *** -------------------------------------------------------------------------
  TssPipedProcess = class ( TObject )
    private
      FParameters        : string;
      FRunCommand        : string;
      FRunType           : TssRunType;

      FInputPipeRead     : THandle;
      FErrorPipeWrite    : THandle;
      FInputPipeWrite    : THandle;
      FOutputPipeRead    : THandle;
      FErrorPipeRead     : THandle;
      FOutputPipeWrite   : THandle;

      FsaSecurity        : TSecurityAttributes;            
      FsuiStartup        : TStartupInfo;
      FpiProcess         : TProcessInformation;

      FInputPipeCreated  : Boolean;
      FOutputPipeCreated : Boolean;
      FErrorPipeCreated  : Boolean;

      FProcessCreated    : Boolean;
      FShowWindow        : Boolean;
      FErrorToOutput     : Boolean;

      FRunUsername: string;
      FRunPassword: string;
      FUserToken  : THandle;

      {$IFDEF USEJWA}
      {$ENDIF}
      
      function PerformLogon( const AUsername, APassword, ADomain: string ): THandle;
      
    public
      constructor Create();
      destructor Destroy; override;

      procedure Initialise();
      procedure Finalise();

      procedure CreateTheProcess();
      procedure TerminateTheProcess();

      function IsStillRunning(): Boolean;
      function ProcessID(): Cardinal;

      procedure WriteToInputPipe( const ABuffer: string );
      function  ReadFromOutputPipe(): string;

      property RunCommand : string    read FRunCommand  write FRunCommand;
      property Parameters : string    read FParameters  write FParameters;

      property RunUsername: string read FRunUsername write FRunUsername;
      property RunPassword: string read FRunPassword write FRunPassword;

      {$IFDEF USEJWA}
      {$ENDIF}

      property RunType      : TssRunType  read FRunType     write FRunType;
      property ShowWindow   : Boolean   read FShowWindow  write FShowWindow;
      property ErrorToOutput: Boolean read FErrorToOutput write FErrorToOutput;

      // *** Pipe Handles
      property InputPipeRead   : THandle   read FInputPipeRead   write FInputPipeRead;
      property InputPipeWrite  : THandle   read FInputPipeWrite  write FInputPipeWrite;

      property OutputPipeRead  : THandle   read FOutputPipeRead  write FOutputPipeRead;
      property OutputPipeWrite : THandle   read FOutputPipeWrite write FOutputPipeWrite;

      property ErrorPipeRead   : THandle   read FErrorPipeRead   write FErrorPipeRead;
      property ErrorPipeWrite  : THandle   read FErrorPipeWrite  write FErrorPipeWrite;
  end;

implementation

uses
  SysUtils;

{ TssPipedProcess }

{-------------------------------------------------------------------------------
  Procedure: TssPipedProcess.Create
  Author:    bvonfintel
  DateTime:  2015.01.15
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
constructor TssPipedProcess.Create;
begin
  inherited;

  FInputPipeCreated   := False;
  FOutputPipeCreated  := False;
  FErrorPipeCreated   := False;
  FShowWindow         := True;
  FErrorToOutput      := False;
  FRunType            := rtCurrentUser;
end;

{-------------------------------------------------------------------------------
  Procedure: TssPipedProcess.CreateTheProcess
  Author:    bvonfintel
  DateTime:  2015.01.15
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
procedure TssPipedProcess.CreateTheProcess;
var
  LCommand : string;
begin
  LCommand := FRunCommand + ' ' + FParameters;

  case FRunType of
    // *** -------------------------------------------------------------------------
    // *** Current user
    // *** -------------------------------------------------------------------------
    rtCurrentUser : begin
      FProcessCreated := CreateProcess( nil,
                                        PChar( LCommand ),
                                        @FsaSecurity,
                                        @FsaSecurity,
                                        True,
                                        0,
                                        nil,
                                        nil,
                                        FsuiStartup,
                                        FpiProcess );
    end;
    // *** -------------------------------------------------------------------------
    // *** Specify a username and password
    // *** -------------------------------------------------------------------------
    rtSpecifiedUser : begin
      FUserToken := PerformLogon( FRunUsername, FRunPassword, '' );
      
      FProcessCreated := CreateProcessAsUser( FUserToken,
                                              nil,
                                              PChar( LCommand ),
                                              @FsaSecurity,
                                              @FsaSecurity,
                                              True,
                                              0,
                                              nil,
                                              nil,
                                              FsuiStartup,
                                              FpiProcess );
    end;
    // *** -------------------------------------------------------------------------
    // *** Console User
    // *** -------------------------------------------------------------------------
    {$IFDEF USEJWA}
    rtConsoleUser : begin
      if ( not WTSQueryUserToken( WTSGetActiveConsoleSessionId(), FUserToken ) ) then Exit;

      FProcessCreated := CreateProcessAsUser( FUserToken,
                                              nil,
                                              PChar( LCommand ),
                                              @FsaSecurity,
                                              @FsaSecurity,
                                              True,
                                              0,
                                              nil,
                                              nil,
                                              FsuiStartup,
                                              FpiProcess );
      end;
    {$ENDIF}
  end;  
end;

{-------------------------------------------------------------------------------
  Procedure: TssPipedProcess.Destroy
  Author:    bvonfintel
  DateTime:  2015.01.15
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
destructor TssPipedProcess.Destroy;
begin

  inherited;
end;

{-------------------------------------------------------------------------------
  Procedure: TssPipedProcess.Finalise
  Author:    bvonfintel
  DateTime:  2015.01.15
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
procedure TssPipedProcess.Finalise;
begin
  if ( FInputPipeCreated ) then begin
    CloseHandle( FInputPipeRead );
    CloseHandle( FInputPipeWrite );
    FInputPipeCreated := False;
  end;

  if ( FOutputPipeCreated ) then begin
    CloseHandle( FOutputPipeRead );
    CloseHandle( FOutputPipeWrite );
    FOutputPipeCreated := False;
  end;

  if ( FErrorPipeCreated ) then begin
    CloseHandle( FErrorPipeRead );
    CloseHandle( FErrorPipeWrite );
    FErrorPipeCreated := False;
  end;

  if ( FProcessCreated ) then begin
    CloseHandle( FpiProcess.hProcess);
    CloseHandle( FpiProcess.hThread );
  end;

end;

{-------------------------------------------------------------------------------
  Procedure: TssPipedProcess.Initialise
  Author:    bvonfintel
  DateTime:  2015.01.15
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
procedure TssPipedProcess.Initialise;
begin
  FsaSecurity.nLength               := SizeOf( TSecurityAttributes );
  FsaSecurity.bInheritHandle        := True;
  FsaSecurity.lpSecurityDescriptor  := nil;

  FInputPipeCreated  := CreatePipe( FInputPipeRead,   FInputPipeWrite,  @FsaSecurity, 4096 );
  FOutputPipeCreated := CreatePipe( FOutputPipeRead,  FOutputPipeWrite, @FsaSecurity, 4096 );
  FErrorPipeCreated  := CreatePipe( FErrorPipeRead,   FErrorPipeWrite,  @FsaSecurity, 4096 );

  FillChar( FsuiStartup, SizeOf( FsuiStartup ), #0 );
  FsuiStartup.cb := SizeOf( FsuiStartup );

  FsuiStartup.hStdInput   := FInputPipeRead;
  FsuiStartup.hStdOutput  := FOutputPipeWrite;
  if ( FErrorToOutput ) then
    FsuiStartup.hStdError   := FOutputPipeWrite
  else
    FsuiStartup.hStdError   := FErrorPipeWrite;

  FsuiStartup.dwFlags     := STARTF_USESTDHANDLES + STARTF_USESHOWWINDOW;
  if ( FShowWindow ) then
    FsuiStartup.wShowWindow := SW_SHOWNORMAL
  else
    FsuiStartup.wShowWindow := SW_HIDE;

  FillChar( FpiProcess, SizeOf( TProcessInformation ), #0 );
end;

{-------------------------------------------------------------------------------
  Procedure: TssPipedProcess.IsStillRunning
  Author:    bvonfintel
  DateTime:  2015.01.15
  Arguments: None
  Result:    Boolean
-------------------------------------------------------------------------------}
function TssPipedProcess.IsStillRunning: Boolean;
begin
  Result := WaitForSingleObject( FpiProcess.hProcess, 10 ) = WAIT_TIMEOUT;

end;

{-------------------------------------------------------------------------------
  Procedure: TssPipedProcess.PerformLogon
  Author:    bvonfintel
  DateTime:  2015.01.15
  Arguments: const AUsername, APassword, ADomain: string
  Result:    THandle
-------------------------------------------------------------------------------}
function TssPipedProcess.PerformLogon(const AUsername, APassword, ADomain: string): THandle;
begin
  if ( not LogonUser( PChar( AUsername ), PChar( ADomain ), PChar( APassword ), LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, Result ) ) then
    RaiseLastOSError();  
end;

{-------------------------------------------------------------------------------
  Procedure: TssPipedProcess.ProcessID
  Author:    bvonfintel
  DateTime:  2015.01.15
  Arguments: None
  Result:    Cardinal
-------------------------------------------------------------------------------}
function TssPipedProcess.ProcessID: Cardinal;
begin
  Result := FpiProcess.dwProcessId;
end;

{-------------------------------------------------------------------------------
  Procedure: TssPipedProcess.ReadFromOutputPipe
  Author:    bvonfintel
  DateTime:  2015.01.15
  Arguments: None
  Result:    string
-------------------------------------------------------------------------------}
function TssPipedProcess.ReadFromOutputPipe: string;
const
  CReadBufferSize = 40000;
  CR = AnsiChar($0D);
  LF = AnsiChar($0A);
  NullChar = AnsiChar($00);
var
  LBuffer               : PAnsiChar;
  LBufferAsString       : AnsiString;
  LBytesRead            : DWORD;
  LBytesLeftThisMessage : Integer;
  LTotalBytesAvail      : Integer;
  LThisChar             : AnsiChar;
  LPrevChar             : AnsiChar;
  LPrevPrevChar         : AnsiChar;
  LLineToAdd            : string;
  LIndex                : integer;

begin
  Result := '';
  LBuffer  := AllocMem(CReadBufferSize + 1);
  try
    if ( not PeekNamedPipe( FOutputPipeRead, @LBuffer[0], CReadBufferSize, @LBytesRead, @LTotalBytesAvail, @LBytesLeftThisMessage ) ) then
      Exit;

    if ( LBytesRead > 0 ) then begin

      // *** We have a valid pipe
      Windows.ReadFile( FOutputPipeRead, LBuffer[0], LBytesRead, LBytesRead, nil );
      LBuffer[LBytesRead] := #0;
      LBufferAsString := LBuffer;

      Result := LBufferAsString;
    end;
  finally
    FreeMem( LBuffer );
  end;
end;

{-------------------------------------------------------------------------------
  Procedure: TssPipedProcess.TerminateTheProcess
  Author:    bvonfintel
  DateTime:  2015.01.15
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
procedure TssPipedProcess.TerminateTheProcess;
begin
  TerminateProcess( FpiProcess.hProcess, 0 );
end;

{-------------------------------------------------------------------------------
  Procedure: TssPipedProcess.WriteToInputPipe
  Author:    bvonfintel
  DateTime:  2015.01.15
  Arguments: const ABuffer: string
  Result:    None
-------------------------------------------------------------------------------}
procedure TssPipedProcess.WriteToInputPipe(const ABuffer: string);
var
  LAnsiBuf      : AnsiString;
  LBytesWritten : Cardinal;
begin
  LAnsiBuf := String( ABuffer ) + #13#10;
  Windows.WriteFile( FInputPipeWrite, LAnsiBuf[1], Length( LAnsiBuf ), LBytesWritten, nil );
end;

end.
