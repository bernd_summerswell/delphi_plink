object frmMain: TfrmMain
  Left = 501
  Top = 376
  Width = 642
  Height = 430
  Caption = 'Delphi PLink Test'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object txtSSHHost: TLabeledEdit
    Left = 8
    Top = 24
    Width = 297
    Height = 21
    EditLabel.Width = 44
    EditLabel.Height = 13
    EditLabel.Caption = 'SSH Host'
    TabOrder = 0
  end
  object txtSSHUsername: TLabeledEdit
    Left = 8
    Top = 64
    Width = 297
    Height = 21
    EditLabel.Width = 70
    EditLabel.Height = 13
    EditLabel.Caption = 'SSH Username'
    TabOrder = 1
  end
  object txtSSHPassword: TLabeledEdit
    Left = 8
    Top = 112
    Width = 297
    Height = 21
    EditLabel.Width = 68
    EditLabel.Height = 13
    EditLabel.Caption = 'SSH Password'
    PasswordChar = '*'
    TabOrder = 2
  end
  object txtLocalHost: TLabeledEdit
    Left = 328
    Top = 24
    Width = 145
    Height = 21
    EditLabel.Width = 49
    EditLabel.Height = 13
    EditLabel.Caption = 'Local Host'
    TabOrder = 3
    Text = 'localhost'
  end
  object txtLocalPort: TLabeledEdit
    Left = 480
    Top = 24
    Width = 145
    Height = 21
    EditLabel.Width = 47
    EditLabel.Height = 13
    EditLabel.Caption = 'Local Port'
    TabOrder = 4
    Text = '3389'
  end
  object txtRemoteHost: TLabeledEdit
    Left = 328
    Top = 64
    Width = 145
    Height = 21
    EditLabel.Width = 62
    EditLabel.Height = 13
    EditLabel.Caption = 'Remote Host'
    TabOrder = 5
    Text = 'localhost'
  end
  object txtRemotePort: TLabeledEdit
    Left = 480
    Top = 64
    Width = 145
    Height = 21
    EditLabel.Width = 60
    EditLabel.Height = 13
    EditLabel.Caption = 'Remote Port'
    TabOrder = 6
    Text = '10000'
  end
  object btnLocalForward: TBitBtn
    Left = 8
    Top = 152
    Width = 217
    Height = 41
    Caption = 'DO LOCAL FORWARD'
    TabOrder = 7
    OnClick = btnLocalForwardClick
  end
  object btnRemoteForward: TBitBtn
    Left = 232
    Top = 152
    Width = 217
    Height = 41
    Caption = 'DO REMOTE FORWARD'
    TabOrder = 8
    OnClick = btnRemoteForwardClick
  end
  object Memo1: TMemo
    Left = 8
    Top = 208
    Width = 617
    Height = 193
    Lines.Strings = (
      'Memo1')
    TabOrder = 9
  end
  object btnStop: TBitBtn
    Left = 456
    Top = 152
    Width = 97
    Height = 41
    Caption = 'STOP'
    TabOrder = 10
    OnClick = btnStopClick
  end
  object tmrCheckProcess: TTimer
    Enabled = False
    OnTimer = tmrCheckProcessTimer
    Left = 456
    Top = 104
  end
end
