# Delphi Plink Wrapper #

This is a Delphi Wrapper around the Plink command line tool that is included with Putty (http://www.chiark.greenend.org.uk/~sgtatham/putty/).

Details of the command line options can be found here:
http://the.earth.li/~sgtatham/putty/0.58/htmldoc/Chapter7.html

### Example ###

An example application is included (in Delphi 7).

Basic example would be as follows:

```
#!delphi

FTunnel := TssPlinkTunnel.Create();
FTunnel.SSHHost       := 'example.com';
FTunnel.SSHUsername   := 'myuser';
FTunnel.SSHPassword   := 'mypass';
FTunnel.RemoteForward( ‘localhost', '3389', 'localhost', '10000' );

```
This would forward our local port 3389 and open up a port (10000) on the server.

You can monitor whether the a spawned process is still running:


```
#!delphi

if ( not FTunnel.IsRunning() ) then
  Memo1.Lines.Add( 'Disconnect detected' );

```