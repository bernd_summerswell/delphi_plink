{*******************************************************}
{ UNIT: FMain.pas                                       }                          
{ Delphi PLink                                          }
{                                                       }
{ Copyright (C) 2016 SummerSwell                        }
{ Author  : bvonfintel                                  } 
{ Created : 2016/10/03 07:23:54 PM                      }
{                                                       }
{*******************************************************}
unit FMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,
  ssPipedProcess, StdCtrls, Buttons, ExtCtrls,
  ssPlinkTunnel;

type
  TfrmMain = class(TForm)
    txtSSHHost: TLabeledEdit;
    txtSSHUsername: TLabeledEdit;
    txtSSHPassword: TLabeledEdit;
    txtLocalHost: TLabeledEdit;
    txtLocalPort: TLabeledEdit;
    txtRemoteHost: TLabeledEdit;
    txtRemotePort: TLabeledEdit;
    btnLocalForward: TBitBtn;
    btnRemoteForward: TBitBtn;
    Memo1: TMemo;
    btnStop: TBitBtn;
    tmrCheckProcess: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnRemoteForwardClick(Sender: TObject);
    procedure btnLocalForwardClick(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure tmrCheckProcessTimer(Sender: TObject);
  private
    { Private declarations }
    procedure TunnelOnDebug( const ADebugMessage: string );
  public
    { Public declarations }
    FTunnel : TssPlinkTunnel;
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}


{-------------------------------------------------------------------------------
  Procedure: TfrmMain.btnLocalForwardClick
  Author:    bvonfintel
  DateTime:  2016.10.03
  Arguments: Sender: TObject
  Result:    None
-------------------------------------------------------------------------------}
procedure TfrmMain.btnLocalForwardClick(Sender: TObject);
begin
  FTunnel.SSHHost       := txtSSHHost.Text;
  FTunnel.SSHUsername   := txtSSHUsername.Text;
  FTunnel.SSHPassword   := txtSSHPassword.Text;

  FTunnel.OnDebug       := Self.TunnelOnDebug;

  FTunnel.LocalForward( txtRemoteHost.Text, txtRemotePort.Text, txtLocalHost.Text, txtLocalPort.Text );

  tmrCheckProcess.Enabled := True;
end;

{-------------------------------------------------------------------------------
  Procedure: TfrmMain.BitBtn1Click
  Author:    bvonfintel
  DateTime:  2016.10.03
  Arguments: Sender: TObject
  Result:    None
-------------------------------------------------------------------------------}
procedure TfrmMain.btnRemoteForwardClick(Sender: TObject);
begin
  FTunnel.SSHHost       := txtSSHHost.Text;
  FTunnel.SSHUsername   := txtSSHUsername.Text;
  FTunnel.SSHPassword   := txtSSHPassword.Text;

  FTunnel.OnDebug       := Self.TunnelOnDebug;

  FTunnel.RemoteForward( txtLocalHost.Text, txtLocalPort.Text, txtRemoteHost.Text, txtRemotePort.Text );

  tmrCheckProcess.Enabled := True;
end;


{-------------------------------------------------------------------------------
  Procedure: TfrmMain.btnStopClick
  Author:    bvonfintel
  DateTime:  2016.10.03
  Arguments: Sender: TObject
  Result:    None
-------------------------------------------------------------------------------}
procedure TfrmMain.btnStopClick(Sender: TObject);
begin
  if ( FTunnel.IsRunning() ) then FTunnel.StopTunnel();
  tmrCheckProcess.Enabled := False;
end;


{-------------------------------------------------------------------------------
  Procedure: TfrmMain.FormCreate
  Author:    bvonfintel
  DateTime:  2016.10.03
  Arguments: Sender: TObject
  Result:    None
-------------------------------------------------------------------------------}
procedure TfrmMain.FormCreate(Sender: TObject);
begin
  FTunnel := TssPlinkTunnel.Create();
end;

{-------------------------------------------------------------------------------
  Procedure: TfrmMain.FormDestroy
  Author:    bvonfintel
  DateTime:  2016.10.03
  Arguments: Sender: TObject
  Result:    None
-------------------------------------------------------------------------------}
procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  FTunnel.Free();
end;

{-------------------------------------------------------------------------------
  Procedure: TfrmMain.tmrCheckProcessTimer
  Author:    bvonfintel
  DateTime:  2016.10.03
  Arguments: Sender: TObject
  Result:    None
-------------------------------------------------------------------------------}
procedure TfrmMain.tmrCheckProcessTimer(Sender: TObject);
begin
  tmrCheckProcess.Enabled := False;
  try
    if ( not FTunnel.IsRunning() ) then
      Memo1.Lines.Add( 'Disconnect detected' );

  finally
    tmrCheckProcess.Enabled := True;
  end;
end;



{-------------------------------------------------------------------------------
  Procedure: TfrmMain.TunnelOnDebug
  Author:    bvonfintel
  DateTime:  2016.10.03
  Arguments: const ADebugMessage: string
  Result:    None
-------------------------------------------------------------------------------}
procedure TfrmMain.TunnelOnDebug(const ADebugMessage: string);
begin
  Memo1.Lines.Add( ADebugMessage );
end;




end.
